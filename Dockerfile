FROM node:12.22-alpine3.14

RUN apk add --no-cache \
        python3 \
        py3-pip \
        jq \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*

WORKDIR /usr/src/app


COPY package*.json /usr/src/app/
RUN npm install

COPY . /usr/src/app 
RUN chmod +x /usr/src/app/initialize-container.sh
ENV PORT 3000
EXPOSE $PORT

ENTRYPOINT [ "sh", "-c", "/usr/src/app/initialize-container.sh" ]
