#!/bin/sh

export SSM_USER_NAME=`aws ssm get-parameter --name /app/username --with-decryption --region us-east-2 --query "Parameter"."Value"| sed -e 's/^"//' -e 's/"$//'`
export SSM_PASSWORD=`aws ssm get-parameter --name /app/password --with-decryption --region us-east-2 --query "Parameter"."Value"| sed -e 's/^"//' -e 's/"$//'`
SM=`aws secretsmanager get-secret-value --secret-id /app/test-app --region us-east-2 --query "SecretString" --output text|sed -e 's/^"//' -e 's/"$//'`
export SM_USER_NAME=`echo $SM | jq .user`
export SM_PASSWORD=`echo $SM | jq .password`

npm run start
