var express = require('express');


var app = express();

app.get('/ssm', (req, res) => {
  res.send({
    "username": process.env.SSM_USER_NAME,
    "password": process.env.SSM_PASSWORD,
    "message": "Reading secrets from ssm"
  })
});

app.get('/secret-manager', (req, res) => {
  res.send({
    "username": process.env.SM_USER_NAME,
    "password": process.env.SM_PASSWORD,
    "message": "Reading secrets from secret-manager"
  })
});

app.get('/vault', (req, res) => {
  res.send({
    "username": process.env.VAULT_USER_NAME,
    "password": process.env.VAULT_PASSWORD,
    "message": "Reading secrets from vault"
  })
});

app.listen(3000, () => {
  console.log("Application is running at port: 3000")
});


module.exports = app;
